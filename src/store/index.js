import Vue from 'vue'
import Vuex from 'vuex'
import auth from '@/store/modules/auth'
import documents from '@/store/modules/documents'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'producction'

export default new Vuex.Store({
  modules: {
    auth,
    documents
  },
  strict: debug
})
