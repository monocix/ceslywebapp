import { LOGIN_REQUEST } from '@/store/actionTypes'
import authService from '@/api/auth'
import jwt from '@/common/jwt'
import apiClient from '@/common/apiClient'
import { SET_TOKEN } from '@/store/mutationTypes'
import { getField, updateField } from 'vuex-map-fields'

const state = {
  token: jwt.getToken() || '',
  credentials: {
    ruc: '',
    username: '',
    password: ''
  }
}

const getters = {
  getField,
  isAuthenticated: state => !!state.token
}

const mutations = {
  updateField,

  [SET_TOKEN] (state, token) {
    state.token = token
  }
}

const actions = {
  [LOGIN_REQUEST] ({ commit, state }, credentials) {
    return new Promise((resolve, reject) => {
      authService.login(state.credentials).then(response => {
        commit(SET_TOKEN, response.data.token)
        jwt.setToken(response.data.token)
        apiClient.setHeader()
        resolve()
      }).catch(err => {
        reject(err)
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
