import { COUNTER_DOCUMENTS_REQUEST, DOCUMENTS_REQUEST } from '@/store/actionTypes'
import { SET_COUNTERS, SET_ITEMS } from '@/store/mutationTypes'
import documentsService from '@/api/documents'

const state = {
  counters: [],
  items: []
}

const getters = {

}

const mutations = {
  [SET_COUNTERS] (state, counters) {
    state.counters = counters
  },

  [SET_ITEMS] (state, items) {
    state.items = items
  }
}

const actions = {
  async [COUNTER_DOCUMENTS_REQUEST] ({ commit }) {
    let response = await documentsService.counter()
    commit(SET_COUNTERS, response.data)
  },

  async [DOCUMENTS_REQUEST] ({ commit }, params) {
    let response = await documentsService.getAll(params)
    commit(SET_ITEMS, response.data.data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
