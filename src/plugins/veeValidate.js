import Vue from 'vue'
import VeeValidate from 'vee-validate'

Vue.use(VeeValidate, {
  locale: 'pe',
  dictionary: {
    pe: {
      messages: {
        required: () => 'Este campo es obligatorio.'
      }
    }
  }
})
