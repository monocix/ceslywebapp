import Vue from 'vue'
import '@/plugins/vuetify'
import '@/plugins/veeValidate'
import App from '@/App.vue'
import router from '@/router/index'
import store from '@/store/index'
import '@/registerServiceWorker'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import apiClient from '@/common/apiClient'
import jwt from '@/common/jwt'

Vue.config.productionTip = false

apiClient.init()

if (jwt.getToken()) {
  apiClient.setHeader()
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
