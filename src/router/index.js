import Vue from 'vue'
import Router from 'vue-router'
import TheLogin from '@/views/TheLogin.vue'
import store from '@/store'

Vue.use(Router)

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters['auth/isAuthenticated']) {
    next()
    return
  }
  next('/dashboard')
}

const ifAuthenticated = (to, from, next) => {
  if (store.getters['auth/isAuthenticated']) {
    next()
    return
  }
  next('/auth/login')
}

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/dashboard',
      name: 'dashboard',
      component: () => import('@/views/TheDashboard'),
      beforeEnter: ifAuthenticated,
      children: [
        {
          path: '',
          name: 'counter',
          component: () => import('@/views/TheDashboardCounter')
        },
        {
          path: 'documents',
          name: 'documents',
          component: () => import('@/views/TheDashboardDocuments')
        }
      ]
    },
    {
      path: '/auth/login',
      name: 'login',
      component: TheLogin,
      beforeEnter: ifNotAuthenticated
    }
  ]
})
