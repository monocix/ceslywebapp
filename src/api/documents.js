import apiCliente from '@/common/apiClient'


const DocumentsService = {
  counter () {
    return apiCliente.get('/documents/counter')
  },

  getAll (params) {
    return apiCliente.query('/documents', params)
  }
}

export default DocumentsService
