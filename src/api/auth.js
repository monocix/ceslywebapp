import ApiCliente from '@/common/apiClient'

const AuthService = {
  login (params) {
    return ApiCliente.post('/users/login', params)
  }
}

export default AuthService
