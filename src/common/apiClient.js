import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { API_URL } from '@/common/config'
import { parseToJSON } from '@/common/utils'
import jwt from '@/common/jwt'

const ApiClient = {
  init () {
    Vue.use(VueAxios, axios)
    Vue.axios.defaults.baseURL = API_URL
  },

  setHeader () {
    Vue.axios.defaults.headers.common['Authorization'] = `Bearer ${jwt.getToken()}`
  },

  query (resource, params) {
    return Vue.axios.get(`${resource}`, { params: parseToJSON(params) })
  },

  get (resource, slug = '') {
    return Vue.axios.get(`${resource}/${slug}`)
  },

  post (resource, params) {
    return Vue.axios.post(`${resource}`, parseToJSON(params))
  },

  update (resource, slug, params) {
    return Vue.axios.put(`${resource}/${slug}`, parseToJSON(params))
  },

  put (resource, params) {
    return Vue.axios.put(`${resource}`, parseToJSON(params))
  },

  delete (resource) {
    return Vue.axios.delete(resource)
  }
}

export default ApiClient
